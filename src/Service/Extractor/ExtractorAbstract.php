<?php


namespace App\Service\Extractor;


abstract class ExtractorAbstract
{
    private $curl;

    public function __construct()
    {
        $this->curl = curl_init();
    }

    public function __destruct()
    {
        curl_close($this->curl);
    }


    protected function getWebsiteContent(string $url): string
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_HEADER, FALSE);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($this->curl, CURLOPT_COOKIESESSION, TRUE);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, TRUE);

        $content = curl_exec($this->curl);

        if (empty($content)) {
            throw new \Exception('IP zostało zablokowane przez lnp');
        }

        return $content;
    }
}